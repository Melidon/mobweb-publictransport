package hu.bme.aut.android.publictransport

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.DatePicker
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import hu.bme.aut.android.publictransport.databinding.ActivityDetailsBinding
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import java.util.*


class DetailsActivity : AppCompatActivity() {

    companion object {
        const val KEY_TRANSPORT_TYPE = "KEY_TRANSPORT_TYPE"

    }

    private lateinit var binding: ActivityDetailsBinding

    private var transportType: Int = -1

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetailsBinding.inflate(layoutInflater)

        setContentView(binding.root)

        transportType = this.intent.getIntExtra(KEY_TRANSPORT_TYPE, -1)

        binding.tvTicketType.text = getTypeString(transportType)

        binding.btnPurchase.setOnClickListener {
            val typeString = getTypeString(transportType)
            val dateString =
                getDateFrom(binding.dpStartDate) + " - " + getDateFrom(binding.dpEndDate)

            val intent = Intent(this, PassActivity::class.java)
            intent.putExtra(PassActivity.KEY_TYPE_STRING, typeString)
            intent.putExtra(PassActivity.KEY_DATE_STRING, dateString)
            startActivity(intent)
        }

        binding.rgPriceCategory.setOnCheckedChangeListener { _, _ -> refreshPrice() }
        binding.dpStartDate.setOnDateChangedListener { _, _, _, _ -> refreshPrice() }
        binding.dpEndDate.setOnDateChangedListener { _, _, _, _ -> refreshPrice() }

        refreshPrice()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun refreshPrice() {
        try {
            val price = getDays() * getPricePerDay() * (100 - getDiscount()) / 100
            binding.tvPrice.text = "$price Ft"
        } catch (e: Exception) {
            // Because it is bad.
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getDays(): Long {
        val endDate = LocalDate.of(
            binding.dpEndDate.year,
            binding.dpEndDate.month,
            binding.dpEndDate.dayOfMonth
        )
        val startDate = LocalDate.of(
            binding.dpStartDate.year,
            binding.dpStartDate.month,
            binding.dpStartDate.dayOfMonth
        )
        return ChronoUnit.DAYS.between(startDate, endDate)
    }

    private fun getPricePerDay(): Int {
        return when (transportType) {
            ListActivity.TYPE_BIKE -> 700
            ListActivity.TYPE_TRAIN -> 1500
            ListActivity.TYPE_BUS -> 1000
            ListActivity.TYPE_BOAT -> 2500
            else -> 0
        }
    }

    private fun getDiscount(): Int {
        return when (binding.rgPriceCategory.checkedRadioButtonId) {
            binding.rbFullPrice.id -> 0
            binding.rbSenior.id -> 90
            binding.rbPublicServant.id -> 50
            else -> 0
        }
    }

    private fun getTypeString(transportType: Int): String {
        return when (transportType) {
            ListActivity.TYPE_BUS -> "Bus pass"
            ListActivity.TYPE_TRAIN -> "Train pass"
            ListActivity.TYPE_BIKE -> "Bike pass"
            ListActivity.TYPE_BOAT -> "Boat pass"
            else -> "Unknown pass type"
        }
    }

    private fun getDateFrom(picker: DatePicker): String {
        return String.format(
            Locale.getDefault(), "%04d.%02d.%02d.",
            picker.year, picker.month + 1, picker.dayOfMonth
        )
    }

}