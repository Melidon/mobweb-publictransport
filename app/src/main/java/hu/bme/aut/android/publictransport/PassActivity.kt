package hu.bme.aut.android.publictransport

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import hu.bme.aut.android.publictransport.databinding.ActivityPassBinding

class PassActivity : AppCompatActivity() {

    companion object {
        const val KEY_DATE_STRING = "KEY_DATE_STRING"
        const val KEY_TYPE_STRING = "KEY_TYPE_STRING"
    }

    private lateinit var binding: ActivityPassBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPassBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.tvTicketType.text = intent.getStringExtra(KEY_TYPE_STRING)
        binding.tvDates.text = intent.getStringExtra(KEY_DATE_STRING)
    }
}