package hu.bme.aut.android.publictransport

import android.content.Intent
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import androidx.appcompat.app.AppCompatActivity
import hu.bme.aut.android.publictransport.databinding.ActivityListBinding

class ListActivity : AppCompatActivity() {

    companion object {
        const val TYPE_BIKE = 1
        const val TYPE_TRAIN = 2
        const val TYPE_BUS = 3
        const val TYPE_BOAT = 4
    }

    private lateinit var binding: ActivityListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityListBinding.inflate(layoutInflater)

        setContentView(binding.root)

        with(binding) {
            btnBike.setOnClickListener {
                val intent = Intent(this@ListActivity, DetailsActivity::class.java)
                intent.putExtra(DetailsActivity.KEY_TRANSPORT_TYPE, TYPE_BIKE)
                startActivity(intent)
            }
            btnBus.setOnClickListener {
                val intent = Intent(this@ListActivity, DetailsActivity::class.java)
                intent.putExtra(DetailsActivity.KEY_TRANSPORT_TYPE, TYPE_TRAIN)
                startActivity(intent)
            }
            btnTrain.setOnClickListener {
                val intent = Intent(this@ListActivity, DetailsActivity::class.java)
                intent.putExtra(DetailsActivity.KEY_TRANSPORT_TYPE, TYPE_BUS)
                startActivity(intent)
            }
            btnBoat.setOnClickListener {
                val intent = Intent(this@ListActivity, DetailsActivity::class.java)
                intent.putExtra(DetailsActivity.KEY_TRANSPORT_TYPE, TYPE_BOAT)
                startActivity(intent)
            }
        }
    }
}