package hu.bme.aut.android.publictransport

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import hu.bme.aut.android.publictransport.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Thread.sleep(3000)
        setTheme(R.style.Theme_PublicTransport)

        binding = ActivityLoginBinding.inflate(layoutInflater)

        setContentView(binding.root)

        binding.btnLogin.setOnClickListener {
            when {
                binding.etEmailAddress.text.toString().isEmpty() -> {
                    binding.etEmailAddress.requestFocus()
                    binding.etEmailAddress.error = "Please enter your email address"
                }
                binding.etPassword.text.toString().isEmpty() -> {
                    binding.etPassword.requestFocus()
                    binding.etPassword.error = "Please enter your email password"
                }
                else -> {
                    startActivity(Intent(this, ListActivity::class.java))
                }
            }
        }
    }
}